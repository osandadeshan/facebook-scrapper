import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Project Name    : facebook-scrapper
 * Developer       : Osanda Deshan
 * Version         : 1.0.0
 * Date            : 9/19/2021
 * Time            : 8:50 AM
 * Description     :
 **/

public class FacebookTest {

    private WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(60, SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.facebook.com/");
    }

    @Test
    public void testLogin() {
        driver.findElement(By.id("email")).sendKeys("afafa@sha.sgs");
        driver.findElement(By.id("pass")).sendKeys("davidjones");
        driver.findElement(By.name("login")).click();
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[text()='Afef Lamouchi Jouini']"))));
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
